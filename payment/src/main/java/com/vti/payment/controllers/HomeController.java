package com.vti.payment.controllers;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class HomeController {

    @Value("${message-payment}")
    private String message;

    @GetMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    public String welcome() {
        return message;
    }
}
